package au.com.digitalpurpose.betterworld.server.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.digitalpurpose.betterworld.server.model.Idea;
import au.com.digitalpurpose.betterworld.server.repository.IdeaRepository;
import au.com.digitalpurpose.betterworld.server.repository.IdeaSpecification;
import au.com.digitalpurpose.betterworld.server.request.idea.CreateIdeaRequest;

@Service
public class IdeaService {

    private final IdeaRepository ideaRepository;

    @Autowired
    public IdeaService(IdeaRepository ideaRepository) {
        this.ideaRepository = ideaRepository;
    }

    public Collection<Idea> findIdeas(String searchTerm, String goal) {
        return ideaRepository.findAll(IdeaSpecification.createSearchQuery(searchTerm, goal));
    }

    public Idea getIdea(Long id) {
        return ideaRepository.findById(id).orElseThrow(() -> new RuntimeException("No idea found for ID " + id));
    }

    public Idea createIdea(CreateIdeaRequest request) {
        Idea newIdea = new Idea();
        newIdea.setTitle(request.getTitle());
        newIdea.setDescription(request.getDescription());
        newIdea.setThumbnailUrl(request.getImageUrl());
        newIdea.setSubmitterName(request.getSubmitterName());
        return ideaRepository.save(newIdea);
    }
}
