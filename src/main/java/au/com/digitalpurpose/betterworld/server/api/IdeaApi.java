package au.com.digitalpurpose.betterworld.server.api;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.com.digitalpurpose.betterworld.server.model.Idea;
import au.com.digitalpurpose.betterworld.server.request.idea.CreateIdeaRequest;
import au.com.digitalpurpose.betterworld.server.service.IdeaService;

@RestController
@RequestMapping("/api/idea")
public class IdeaApi {

    private final IdeaService ideaService;

    @Autowired
    public IdeaApi(IdeaService ideaService) {
        this.ideaService = ideaService;
    }

    @GetMapping
   public Collection<Idea> findIdeas(@RequestParam(value = "searchTerm", required = false) String searchTerm,
                                     @RequestParam(value = "goal", required = false) String goal) {
      return ideaService.findIdeas(searchTerm, goal);
   }

    @GetMapping("{id}")
    public Idea getIdea(@PathVariable(value = "id") long id) {
        return ideaService.getIdea(id);
    }

    @PostMapping
    public Idea createIdea(@RequestBody CreateIdeaRequest request) {
        return ideaService.createIdea(request);
    }
}
