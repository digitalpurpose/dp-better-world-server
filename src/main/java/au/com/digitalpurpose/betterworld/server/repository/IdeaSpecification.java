package au.com.digitalpurpose.betterworld.server.repository;

import au.com.digitalpurpose.betterworld.server.model.Idea;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;

public class IdeaSpecification {

    public static Specification<Idea> createSearchQuery(final String searchTerm, final String goal) {
        return (idea, query, builder) -> {
            Predicate conjunction = builder.conjunction();

            if (StringUtils.isNotBlank(searchTerm)) {
                String lowerCaseMatchTerm = searchTerm.toLowerCase();
                String[] words = lowerCaseMatchTerm.split(" ");
                for (String word : words) {
                    String likeMatchTerm = "%" + word + "%";
                    conjunction.getExpressions().add(
                            builder.and(
                                    builder.like(builder.lower(idea.get("title")), likeMatchTerm),
                                    builder.like(builder.lower(idea.get("description")), likeMatchTerm)
                            )
                    );
                }
            }

            if (StringUtils.isNotBlank(goal)) {
                conjunction.getExpressions().add(builder.equal(idea.get("goal"), goal));
            }

            return conjunction;
        };
    }

}
